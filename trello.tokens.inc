<?php

/**
 * @file
 * Builds placeholder replacement tokens Trello data.
 */

/**
 * Implements hook_token_info().
 */
function trello_token_info() {
  $card_type = 'trello_' . TRELLO_TYPE_CARD;
  $list_type = 'trello_' . TRELLO_TYPE_LIST;
  $board_type = 'trello_' . TRELLO_TYPE_BOARD;

  // Define token types.
  $types[$card_type] = array(
    'name' => t('Trello'),
    'description' => t("Card's tokens available from Trello."),
  );
  $types[$list_type] = array(
    'name' => t('Trello'),
    'description' => t("List's tokens available from Trello."),
  );
  $types[$board_type] = array(
    'name' => t('Trello'),
    'description' => t("Board's tokens available from Trello."),
  );

  // Card's tokens.
  $cards['id'] = array(
    'name' => t('ID'),
    'description' => t('ID of card.'),
  );
  $cards['type'] = array(
    'name' => t('Type'),
    'description' => t('Card.'),
  );
  $cards['source'] = array(
    'name' => t('Item source'),
    'description' => t('Trello.'),
  );
  $cards['name'] = array(
    'name' => t('Name'),
    'description' => t('Name of card.'),
  );
  $cards['shortLink'] = array(
    'name' => t('shortLink'),
    'description' => t('Short link to card.'),
  );
  $cards['idShort'] = array(
    'name' => t('idShort'),
    'description' => t('Short link id.'),
  );
  $cards['idList'] = array(
    'name' => t('idList'),
    'description' => t('Parent list id.'),
  );
  $cards['list'] = array(
    'name' => t('list'),
    'description' => t('Parent list name.'),
  );
  $cards['board'] = array(
    'name' => t('board'),
    'description' => t('Parent board name.'),
  );
  $cards['idBoard'] = array(
    'name' => t('Board id'),
    'description' => t('Parent board id.'),
  );
  $cards['pos'] = array(
    'name' => t('pos'),
    'description' => t('Position in list.'),
  );
  $cards['closed'] = array(
    'name' => t('closed'),
    'description' => t('Flag "archived".'),
  );
  $cards['old_key'] = array(
    'name' => t('old_key'),
    'description' => t('What field was updated.'),
  );
  $cards['old_value'] = array(
    'name' => t('old_value'),
    'description' => t('Old value of updated field.'),
  );

  // List's tokens.
  $lists['id'] = array(
    'name' => t('ID'),
    'description' => t('List id.'),
  );
  $lists['type'] = array(
    'name' => t('Type'),
    'description' => t('List.'),
  );
  $lists['source'] = array(
    'name' => t('Item source'),
    'description' => t('Trello.'),
  );
  $lists['name'] = array(
    'name' => t('Name'),
    'description' => t('List name.'),
  );
  $lists['idBoard'] = array(
    'name' => t('idBoard'),
    'description' => t('Parent board id'),
  );
  $lists['closed'] = array(
    'name' => t('closed'),
    'description' => t('Flag "archived"'),
  );
  $lists['old_key'] = array(
    'name' => t('old_key'),
    'description' => t('What field was updated'),
  );
  $lists['old_value'] = array(
    'name' => t('old_value'),
    'description' => t('Old value of updated field'),
  );

  // Board's tokens.
  $boards['id'] = array(
    'name' => t('ID'),
    'description' => t('Board id.'),
  );
  $boards['type'] = array(
    'name' => t('type'),
    'description' => t('Board.'),
  );
  $boards['source'] = array(
    'name' => t('Item source'),
    'description' => t('Trello.'),
  );
  $boards['name'] = array(
    'name' => t('Name'),
    'description' => t('Board name.'),
  );
  $boards['shortLink'] = array(
    'name' => t('shortLink'),
    'description' => t('Short link to card.'),
  );
  $boards['closed'] = array(
    'name' => t('closed'),
    'description' => t('Flag "archived"'),
  );
  $boards['old_key'] = array(
    'name' => t('old_key'),
    'description' => t('What field was updated'),
  );
  $boards['old_value'] = array(
    'name' => t('old_value'),
    'description' => t('Old value of updated field'),
  );

  return array(
    'types' => $types,
    'tokens' => array(
      $card_type => $cards,
      $list_type => $lists,
      $board_type => $boards,
    ),
  );
}

/**
 * Implements hook_tokens().
 */
function trello_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $replacements = array();

  // Do not handle any other types of tokens.
  if (strpos($type, 'trello_') === FALSE) {
    return $replacements;
  }

  $data = $data[$type];

  // Card's tokens.
  if ($type == 'trello_' . TRELLO_TYPE_CARD) {
    foreach ($tokens as $name => $original) {
      // Card tokens.
      switch ($name) {
        case 'id':
        case 'name':
        case 'idShort':
        case 'shortLink':
        case 'idList':
        case 'pos':
        case 'closed':
          $replacements[$original] = $data[TRELLO_TYPE_CARD][$name];
          break;

        case 'old_key':
          $replacements[$original] = key($data['old']);
          break;

        case 'old_value':
          $replacements[$original] = $data['old'][key($data['old'])];
          break;

        case 'list':
          if (isset($data[TRELLO_TYPE_LIST])) {
            $replacements[$original] = $data[TRELLO_TYPE_LIST]['name'];
          }
          elseif (isset($data['listAfter'])) {
            $replacements[$original] = $data['listAfter']['name'];
          }
          break;

        case 'board':
          $replacements[$original] = $data[TRELLO_TYPE_BOARD]['name'];
          break;

        case 'idBoard':
          $replacements[$original] = $data[TRELLO_TYPE_BOARD]['id'];
          break;

        case 'type':
          $replacements[$original] = TRELLO_TYPE_CARD;
          break;

        case 'source':
          $replacements[$original] = 'trello';
          break;
      }
    }
  }
  // List tokens.
  elseif ($type == 'trello_' . TRELLO_TYPE_LIST) {
    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'id':
        case 'name':
        case 'closed':
          $replacements[$original] = $data[TRELLO_TYPE_LIST][$name];
          break;

        case 'old_key':
          $replacements[$original] = key($data['old']);
          break;

        case 'old_value':
          $replacements[$original] = $data['old'][key($data['old'])];
          break;

        case 'type':
          $replacements[$original] = TRELLO_TYPE_LIST;
          break;

        case 'source':
          $replacements[$original] = 'trello';
          break;
      }
    }
  }
  // Board tokens.
  elseif ($type == 'trello_' . TRELLO_TYPE_BOARD) {
    foreach ($tokens as $name => $original) {

      // Proceed with default, simple fields.
      switch ($name) {
        case 'id':
        case 'name':
        case 'shortLink':
        case 'closed':
          $replacements[$original] = $data[TRELLO_TYPE_BOARD][$name];
          break;

        case 'old_key':
          $replacements[$original] = key($data['old']);
          break;

        case 'type':
          $replacements[$original] = TRELLO_TYPE_BOARD;
          break;

        case 'old_value':
          $replacements[$original] = $data['old'][key($data['old'])];
          break;

        case 'source':
          $replacements[$original] = 'trello';
          break;
      }
    }
  }
  return $replacements;
}
